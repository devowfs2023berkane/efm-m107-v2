<?php

namespace App\Http\Controllers;

use App\Models\Diplome;
use App\Models\OffreEmploi;
use App\Models\SecteurActivite;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OffreEmploiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
//        $offres = OffreEmploi::with('diplome', 'secteurActivite')->paginate(5);
        $offres = OffreEmploi::with('diplome', 'secteurActivite')->get();

        return Inertia::render('Offres/Index', compact('offres'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
//        $diplomes = \App\Models\Diplome::all();
//        $secteurs = \App\Models\SecteurActivite::all();
        $diplomes = Diplome::select('id', 'nom')->get();
        $secteurs = SecteurActivite::select('id', 'nom')->get();
        return Inertia::render('Offres/Create', compact('diplomes', 'secteurs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $fields = $request->validate([
            'titre' => 'required|max:100',
            'description' => 'required|max:200',
            'diplome_id' => 'required|exists:diplomes,id',
            'secteur_activite_id' => 'required|exists:secteur_activites,id',
            'datePublication' => 'required|date'
        ]);
        $offre = OffreEmploi::create($request->all());
        return redirect()->route('offres.show', $offre)->with('message', 'Offre a ete crée avec succès !');
    }

    /**
     * Display the specified resource.
     */
    public function show(OffreEmploi $offre)
    {
        return Inertia::render('Offres/Show', [
            'offre' => $offre->load('diplome', 'secteurActivite')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OffreEmploi $offreEmploi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, OffreEmploi $offreEmploi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OffreEmploi $offre)
    {
        $offre->delete();
        return redirect(route('offres.index'))->with('message', 'Offre a ete supprimée avec succès !');
    }
}
