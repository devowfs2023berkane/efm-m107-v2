<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OffreEmploi extends Model
{
    use HasFactory;

    protected $fillable = [
        'titre',
        'description',
        'datePublication',
        'diplome_id',
        'secteur_activite_id',
    ];

    public function diplome()
    {
        return $this->belongsTo(Diplome::class);
    }

    public function secteurActivite()
    {
        return $this->belongsTo(SecteurActivite::class);
    }
}
