# Examen Regional de Fin de Module
_Developpement Digital 1er annee_ `M107` _2023_ v2

## Laravel: Breeze with reactjs

## Etapes d'installation

- Pour Laravel

```bash
composer i 
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan storage:link
```

```bash
php artisan migrate
```

```bash
php artisan serve 
```

- Pour Reactjs

```bash
npm i 
```

```bash
npm run dev 
```

