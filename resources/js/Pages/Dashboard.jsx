import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head} from '@inertiajs/react';

export default function Dashboard({auth, nmbrOffres, offresParSecteur}) {
    return (
        <AuthenticatedLayout
            user={auth.user}
        >
            <Head title="Dashboard"/>

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900">Nombre d'offre d'emploi: {nmbrOffres} </div>
                        <div className="p-6 text-gray-900">Nombre d'offre d'emploi par secteur activite:
                            <table className={'table-auto'}>
                                <thead className={'bg-gray-200 border-b'}>
                                <tr>
                                    <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Secteur d'activite</th>
                                    <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Nombre</th>
                                </tr>
                                </thead>
                                <tbody>
                                {offresParSecteur.map((offre) => (
                                    <tr key={offre.id} className={'bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100'}>
                                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{offre.secteur_activite.nom}</td>
                                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{offre.total}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
