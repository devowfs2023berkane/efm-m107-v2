import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head, router, usePage} from '@inertiajs/react';
import {useState} from "react";
import Alert from "@/Components/Alert.jsx";

export default function Index({auth, offres}) {
    // console.log(offres)
    const {flash} = usePage().props
    const [isAlert, setIsAlert] = useState(false);
    console.log(flash)
    const handleDelete = (e, offre) => {
        e.preventDefault();
        if (confirm('Are you sure you want to delete this offre?')) {
            router.delete(`/offres/${offre.id}`);
            setIsAlert(true);
        }
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
        >
            <Head title="Offres"/>

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    {isAlert && <Alert message={flash.message} setIsAlert={setIsAlert}/>}
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 text-xl">Listes des offres</div>

                        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead
                                className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" className="px-6 py-3">
                                    Identifiant de l'offre
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Nom du secteur d'activite
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Titre de l'offre
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Date de l'offre
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Diplome exige
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {offres.map((offre) => (
                                <tr key={offre.id}>
                                    <td className="px-6 py-4">{offre.id}</td>
                                    <td className="px-6 py-4">{offre.secteur_activite.nom}</td>
                                    <td className="px-6 py-4">{offre.titre}</td>
                                    <td className="px-6 py-4">{offre.datePublication}</td>
                                    <td className="px-6 py-4">{offre.diplome.nom}</td>
                                    <td className="px-6 py-4">
                                        <a href={route('offres.show', offre)}
                                           className={'class="inline-block rounded bg-green-500 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-green-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-green-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] me-3"'}>Detail</a>
                                        <form onSubmit={e => handleDelete(e, offre)}>
                                            <button type="submit"
                                                    className={"inline-block rounded bg-red-500 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-red-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-red-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-red-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"}>Supprimer
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    )
}
