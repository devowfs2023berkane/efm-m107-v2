import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head, Link, router} from '@inertiajs/react';
import {useState} from "react";

export default function Index({auth, diplomes, secteurs}) {
    // console.log(diplomes)
    // console.log(secteurs)
    const [form, setForm] = useState({
        titre: '',
        description: '',
        datePublication: '',
        diplome_id: '',
        secteur_activite_id: '',
    });
    const [error, setError] = useState({
        titre: '',
        description: '',
        datePublication: '',
        diplome_id: '',
        secteur_activite_id: '',
    });

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.id]: e.target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(error)
        console.log(form)
        if (form.titre === '') {
            setError({
                ...error,
                titre: 'Titre est obligatoire'
            });
        } else {
            setError({
                ...error,
                titre: ''
            });
        }

        if (form.datePublication === '') {
            setError({
                ...error,
                datePublication: 'Date de publication est obligatoire'
            });
        } else {
            setError({
                ...error,
                datePublication: ''
            });
        }

        if (form.diplome_id === '') {
            setError({
                ...error,
                diplome_id: 'Diplome est obligatoire'
            });
        } else {
            setError({
                ...error,
                diplome_id: ''
            });
        }

        if (form.secteur_activite_id === '') {
            setError({
                ...error,
                secteur_activite_id: 'Secteur d\'activite est obligatoire'
            });
        } else {
            setError({
                ...error,
                secteur_activite_id: ''
            });
        }

        if (form.description === '') {
            setError({
                ...error,
                description: 'Description est obligatoire'
            });
        } else {
            setError({
                ...error,
                description: ''
            });
        }

        if (error.titre === '' &&
            error.description === '' &&
            error.datePublication === '' &&
            error.diplome_id === '' &&
            error.secteur_activite_id === '') {
            console.log('ok')
            router.post('/offres', form);
        }
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
        >
            <Head title="Dashboard"/>

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 text-xl">Ajouter Nouveau offre</div>
                        <form onSubmit={handleSubmit} className={'p-5'}>
                            <div className="grid gap-6 mb-6 md:grid-cols-2">
                                <div>
                                    <label htmlFor="titre"
                                           className="block mb-2 text-sm font-medium text-gray-900">Titre</label>
                                    <input type="text" id="titre" value={form.titre} onChange={handleChange}
                                           className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                                           placeholder="ex. Alberta" required/>
                                    {error.titre !== '' &&
                                        <p className="text-red-500 text-xs italic border border-2 border-red-500 bg-red-100 p-2 rounded-md mt-1">{error.titre}</p>}
                                </div>
                                <div>
                                    <label htmlFor="datePublication"
                                           className="block mb-2 text-sm font-medium text-gray-900">Date de
                                        publication</label>
                                    <input type="date" id="datePublication" value={form.datePublication}
                                           onChange={handleChange} required
                                           className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    />
                                    {error.datePublication !== '' &&
                                        <p className="text-red-500 text-xs italic border border-2 border-red-500 bg-red-100 p-2 rounded-md mt-1">{error.datePublication}</p>}
                                </div>
                                <div>
                                    <label htmlFor="diplome_id"
                                           className="block mb-2 text-sm font-medium text-gray-900"> Choisi le
                                        diplome </label>
                                    <select id="diplome_id" required
                                            className="block w-full p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                                            value={form.diplome_id} onChange={handleChange}>
                                        <option value="0" hidden>Choisi le diplome</option>
                                        {diplomes.map((diplome) => (
                                            <option value={diplome.id} key={diplome.id}>{diplome.nom}</option>
                                        ))}
                                    </select>
                                    {error.diplome_id !== '' &&
                                        <p className="text-red-500 text-xs italic border border-2 border-red-500 bg-red-100 p-2 rounded-md mt-1">{error.diplome_id}</p>}
                                </div>
                                <div>
                                    <label htmlFor="secteur_activite_id"
                                           className="block mb-2 text-sm font-medium text-gray-900"> Choisi le
                                        secteur d'activite </label>
                                    <select id="secteur_activite_id" required
                                            className="block w-full p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                                            value={form.secteur_activite_id} onChange={handleChange}>
                                        <option value="0" hidden> Choisi le
                                            secteur d'activite
                                        </option>
                                        {secteurs.map((secteur) => (
                                            <option value={secteur.id} key={secteur.id}>{secteur.nom}</option>
                                        ))}
                                    </select>
                                    {error.secteur_activite_id !== '' &&
                                        <p className="text-red-500 text-xs italic border border-2 border-red-500 bg-red-100 p-2 rounded-md mt-1">{error.secteur_activite_id}</p>}
                                </div>
                            </div>
                            <div className="mb-6">
                                <label htmlFor="description"
                                       className="block mb-2 text-sm font-medium text-gray-900">Description</label>
                                <textarea id="description" rows="9" value={form.description} onChange={handleChange}
                                          placeholder="Ajouter une petite description .."
                                          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                          required></textarea>
                                {error.description !== '' &&
                                    <p className="text-red-500 text-xs italic border border-2 border-red-500 bg-red-100 p-2 rounded-md mt-1">{error.description}</p>}
                            </div>
                            <button>
                                <Link href="/offres" className="text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-3 me-2 text-center ">Retour</Link>
                            </button>
                            <button type="submit"
                                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center ">
                                Ajouter
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    )
        ;
}
