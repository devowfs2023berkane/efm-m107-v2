import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head, Link, router} from '@inertiajs/react';

export default function Index({auth, offre}) {
    console.log(offre)

    const handleDelete = (e, offre) => {
        e.preventDefault();
        if (confirm('Are you sure you want to delete this offre?')) {
            router.delete(`/offres/${offre.id}`);
        }
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
        >
            <Head title="Detail d'un offre"/>

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 text-xl">Detail d'un offre</div>

                        <div className="p-6 text-gray-900">
                            <p>
                                <span className="font-bold">Titre d'offre:</span> {offre.titre} <br/>
                                <span className="font-bold">Secteur Nom:</span> {offre.secteur_activite.nom} <br/>
                                <span className="font-bold">publie le :</span> {offre.datePublication} <br/>
                                <span className="font-bold">Diplome demande:</span> {offre.diplome.nom} <br/>
                            </p>
                            <div className="flex mt-5">
                                <button>
                                    <Link href="/offres"
                                          className="text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-3 me-2 text-center ">Retour</Link>
                                </button>
                                <form onSubmit={e => handleDelete(e, offre)}>
                                    <button type="submit"
                                            className={"inline-block rounded bg-red-500 px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-red-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-red-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-red-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"}>Supprimer
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
