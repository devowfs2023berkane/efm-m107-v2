<?php

namespace Database\Factories;

use App\Models\Diplome;
use App\Models\SecteurActivite;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OffreEmploi>
 */
class OffreEmploiFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titre' => $this->faker->word(),
            'description' => $this->faker->text(),
            'datePublication' => $this->faker->date(),
            'diplome_id' => Diplome::all()->random()->id,
            'secteur_activite_id' => SecteurActivite::all()->random()->id,
        ];
    }
}
