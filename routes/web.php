<?php

use App\Http\Controllers\ProfileController;
use App\Models\OffreEmploi;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    // nombre d'offres
    $nmbrOffres = OffreEmploi::count();
    // nombre d'offres par secteur d'activité
    $offresParSecteur = OffreEmploi::with('secteurActivite')->select('secteur_activite_id', DB::raw('count(*) as total'))
        ->groupBy('secteur_activite_id')
        ->get();
    return Inertia::render('Dashboard', compact('nmbrOffres', 'offresParSecteur'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('offres', App\Http\Controllers\OffreEmploiController::class);

});

require __DIR__.'/auth.php';
